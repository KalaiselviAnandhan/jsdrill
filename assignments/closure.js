function counterFactory() {
  // Return an object that has two methods called `increment` and `decrement`.
  // `increment` should increment a counter variable in closure scope and return it.
  // `decrement` should decrement the counter variable and return it.
  var counter=0;
  var increment=function(){
    return counter+=1;
  }
  var decrement=function(){
    return counter-=1;
  }
  return {increment:increment,decrement:decrement}
}
// var counterObj=counterFactory();
// console.log("counterFactory :",counterObj.increment(),counterObj.decrement());

function limitFunctionCallCount(cb, n) {
  // Should return a function that invokes `cb`.
  // The returned function should only allow `cb` to be invoked `n` times.
  // Returning null is acceptable if cb can't be returned
  var count=0;
  var invokeCBnTimes =function (){
    for(let i=1;i<=n;i++){
      console.log(`Call Count: ${cb(i)}`);
      count++;
    }
    return `CallBack Function Called ${count} Times.`;
  }
  return invokeCBnTimes;
}
// var invokefunction=limitFunctionCallCount((element)=>element,3);
// console.log(invokefunction,invokefunction());

function cacheFunction(cb) {
  // Should return a funciton that invokes `cb`.
  // A cache (object) should be kept in closure scope.
  // The cache should keep track of all arguments have been used to invoke this function.
  // If the returned function is invoked with arguments that it has already seen
  // then it should return the cached result and not invoke `cb` again.
  // `cb` should only ever be invoked once for a given set of arguments.
  var obj={};
  var invoke=function(val){
      if(val in obj){
        console.log("use a cache")
        return obj[val];
      }
      else{
        console.log("create a cache")
        let result=cb(val);
        obj[val]=result;
        return result;
      }
  }  
  return invoke;
}
// let result=cacheFunction((e)=>e*e);
// console.log(result(1));
// console.log(result(2));
// console.log(result(1));