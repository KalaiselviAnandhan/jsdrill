const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code. 
/*
  Complete the following functions.
  These functions only need to work with arrays.
  A few of these functions mimic the behavior of the `Built` in JavaScript Array Methods.
  The idea here is to recreate the functions from scratch BUT if you'd like,
  feel free to Re-use any of your functions you build within your other functions.
  **DONT** Use for example. .forEach() to recreate each, and .map() to recreate map etc.
  You CAN use concat, push, pop, etc. but do not use the exact method that you are replicating
*/

function each(elements, cb) {
  // Do NOT use forEach to complete this function.
  // Iterates over a list of elements, yielding each in turn to the `cb` function.
  // This only needs to work with arrays.
  // You should also pass the index into `cb` as the second argument
  // based off http://underscorejs.org/#each

  for(let i=0;i<elements.length;i++){
    cb(elements[i],i);
 }
}
// each(items,(e)=>console.log("Each item",e));

function map(elements, cb) {
  // Do NOT use .map, to complete this function.
  // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
  // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
  // Return the new array.
  var resultarr=[];
  for(let i=0;i<elements.length;i++){
    resultarr.push(cb(elements[i],i))
  }
  return resultarr;
}
// var mapping=map(items,(e,index)=>e*index);
// console.log("Map: ",mapping);

function reduce(elements, cb, startingValue) {
  // Do NOT use .reduce to complete this function.
  // How reduce works: A reduce function combines all elements into a single value going from left to right.
  // Elements will be passed one by one into `cb` along with the `startingValue`.
  // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
  // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
  var currentValue;
  if(startingValue === undefined){
    startingValue = elements[0];
    currentValue = elements[1];
  }
  else{
    currentValue = elements[0];
  }
  var index = elements.indexOf(currentValue);
  for(index;index<elements.length;index++){
    currentValue=elements[index]
    startingValue = cb(startingValue,currentValue)
  }
  return startingValue;
}
// var reducedvalue=reduce(items,(acc,curr)=>acc+curr,0);
// console.log("Reduced Value: ",reducedvalue);


function find(elements, cb) {
  // Do NOT use .includes, to complete this function.
  // Look through each value in `elements` and pass each element to `cb`.
  // If `cb` returns `true` then return that element.
  // Return `undefined` if no elements pass the truth test.
  var result=undefined;
  for(let i=0; i<elements.length;i++){
    var val=cb(elements[i]);
    if(val=== true){
      result= elements[i];
      break;
    }
    else{
      if(i==elements.length-1){
        break;
      }
    }
  }
  return result;
}
// var findval = find(items,(e)=>e<10)
// console.log("Value found: ",findval);

function filter(elements, cb) {
  // Do NOT use .filter, to complete this function.
  // Similar to `find` but you will return an array of all elements that passed the truth test
  // Return an empty array if no elements pass the truth test
  var filteredArr=[]
  for(let i=0; i<elements.length;i++){
    if(cb(elements[i])=== true){
      filteredArr.push(elements[i]);
    }
  }
  return filteredArr;
}
// var mathchesFound=filter(items,(item)=>item%2==0);
// console.log("Filtered Values",mathchesFound);

const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
var flatarr=[];

function flatten(elements) {
  // Flattens a nested array (the nesting can be to any depth).
  // Hint: You can solve this using recursion.
  // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
  for(let i=0; i<elements.length;i++){
    if(Array.isArray(elements[i])==true){
      flatten(elements[i]);
    }
    else{
      flatarr.push(elements[i]);
    }
  }
  return flatarr;
}
// var flat=flatten(nestedArray);
// console.log("Array Flatten : ",flat);