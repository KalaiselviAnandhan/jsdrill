const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.

function keys(obj) {
  // Retrieve all the names of the object's properties.
  // Return the keys as strings in an array.
  // Based on http://underscorejs.org/#keys
  var properties=[];
  for(let pro in obj){
    properties.push(pro);
  }
  return properties;
}
// var key=keys(testObject)
// console.log("Keys :",key);

function values(obj) {
  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values
  var values=[];
  for(let pro in obj){
    values.push(obj[pro]);
  }
  return values;
}
// var value=values(testObject);
// console.log("Values: ",value);

function mapObject(obj, cb) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  // http://underscorejs.org/#mapObject
  for(let pro in obj){
    var val=cb(obj[pro]);
    obj[pro]=val;
  }
  return obj;
}
// var mapping=mapObject(testObject,(element)=>typeof element=="number"?element+1:element+".");
// console.log("Mapping Object: ",mapping);

function pairs(obj) {
  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs
  var keyValuePairs=[];
  for(let pro in obj){
    keyValuePairs.push([pro,obj[pro]]);
  }
  return keyValuePairs;
}
// var keyValuepair=pairs(testObject);
// console.log("Key Value Pair: ",keyValuepair);

/* STRETCH PROBLEMS */

function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert
  var invertObj={};
  for(let pro in obj){
    invertObj[String(obj[pro])]=String(pro);
  }
  return invertObj;
}
// var invertedObj=invert(testObject);
// console.log("InvertedObj: ",invertedObj);

function defaults(obj, defaultProps) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults
  for(let pro in defaultProps){
    if(obj.hasOwnProperty(pro)!==true){
      obj[pro]=defaultProps[pro];
    }
  }
  return obj;
}
// var addDefaultProperty=defaults(testObject,{ name: 'Bruce Wayne', age: 36, location: 'Gotham', gender:"male"});
// console.log("Add Default Property: ",addDefaultProperty);
